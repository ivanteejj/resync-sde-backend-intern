DROP TABLE IF EXISTS Employees;
DROP TABLE IF EXISTS Users;

CREATE TABLE Employees (
    empID int,
    empName varchar(255) NOT NULL,
    empBday date,

    primary key (empID)
);

CREATE TABLE Users (
    username varchar(255),
    userPassword varchar(255),

    primary key (username)
);


INSERT INTO Employees (empID, empName, empBday) VALUES 
(001 , 'Albert', '01/01/1996'),
(002 , 'Becca', '02/01/1996'),
(003 , 'Charles', '03/01/1996'),
(004 , 'Denise', '04/01/1996'); 

 