# Back-end App Test Solution by Ivan Tee

Backend app is developed in express.js framework of course. For database, I used PostGreSQL as I am the most familiar with it. Both the backend and the database have been docker-ized such that you just need to run `docker-compose up --build` on the root folder and both components should work.

## Using the APIs ##
1. `/login`

To use `login`, you will need to make a **POST** request to "site(:port)/login" with the username and password in the body. 
Upon successful login, a **JsonWebToken(JWT)** which lasts 1 hour will be returned. This token is required to access the second and third API.

If the username doesn't exist, an error 404 will be thrown.

If the username exists but the password is wrong, an error 401 will be thrown.

Below is an example of a successful login request.

<img src="./assets/firstAPIExample.PNG" height="500">

2. `/getEmployee/:e_id`

To use `getEmployee/:e_id`, you will need to make a **GET** request to "site(:port)/getEmployee/:e_id" with the aforementioned `token` in the header of the request under the key `x-access-token`.

Upon a successful request, a response with status code 200 along with a JSON object containing the details of the requested employee will be returned.

If no token is provided, an error 403 will be thrown.

If the token is invalid, an error 500 will be thrown.

If there does not exist an employee with the requested employee ID, an error 404 will be thrown.

Finally, if there is any issue with the connection between the backend and the database, an error 500 will be thrown

For example, you can see in the image that the http request is made to `http://localhost:3000/getEmployee/001` to get the information of the employee with ID 001.

<img src="./assets/secondAPIExample.PNG" height="500">

3. `/getEmployees/`

To use `getEmployees/`, you will need to make a **GET** request to "site(:port)/getEmployees/" with the aforementioned `token` in the header of the request under the key `x-access-token`.

Upon a successful request, a response with status code 200 along with a JSON object containing the details of all employees will be returned.

If no token is provided, an error 403 will be thrown.

If the token is invalid, an error 500 will be thrown.

If there does not exist any employee, an error 404 will be thrown.

Finally, if there is any issue with the connection between the backend and the database, an error 500 will be thrown

4. `/register/`

Even though this API was not requested, it was very helpful in helping to test login and it will be helpful for anyone to use to test /login. As such, I've decided to document it here. 

Similar to `/login/`, you will need to make a **POST** request to "site(:port)/register" with the username and password in the body. 

If the user already exists, a response with status code 409 is returned.

## Database

There is just 2 tables in the database namely

1. Users(username, userpassword)
2. Employees(empid, empname, empbday)

As I wasn't sure whether there is a relationship between users and employees, I have implemented these 2 as separate and independent tables.

On the initialization of the database docker container, if there isn't a database already, the sql script under `/database` will run to create the tables and input just a few data for employees. For users, please make use of the `/register/` API to register new users.

## Summary of Docker Containers

| Container Name | Port | Purpose                |
|----------------|------|------------------------|
| db             | 5432 | Database(PostgresSQL)  |
| adminer        | 8080 | Management of Database |
| backend        | 3000 | ExpressJS Server       |