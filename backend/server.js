const express = require('express')
const app = express()

// Used to obtain environment variables
var dotenv = require("dotenv")

// Used to get a connection to the postgres db
const db = require('./db')

// Controller used to handle /login and /registration
const authController = require('./authentication/authController')

// Used to validate tokens before proceeding with the APIs
const validateToken = require('./authentication/validateToken')

// Obtaining basic environment variables
dotenv.config();
const hostname = process.env.HOSTNAME;
const port = process.env.PORT;

// Middleware to help read the body of POST requests - will be required for /login and /register
const bodyParser = require('body-parser');
app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
)

app.listen(port, () => {
  console.log(`Test: Example app listening at http://${hostname}:${port}`)
})

// Post request for /register and /login to be handled by authController
app.post('/register', async (req, res) => authController.register(req, res, db));
app.post('/login', async (req, res) => authController.login(req, res, db));


// Very simple SQL to get all Employees details
const getEmployeesQuery = 
" SELECT * \n" +
"FROM Employees;"

/*
    This /getEmployees API first ensures that there is a valid token before proceeding. Please see validateToken.js for how validateToken works.

    Assuming that there is a valid token, the database is queried to obtian all employee details via the query above. 
    
    If everything goes okay, all the employees details will be returned in a response with status code 200.
    If there is an error in the connect between the database and backend, the backend will respond with an error code of 500.
    If there is no employee in the database, the backend will respond with an status code of 404.
*/
app.get('/getEmployees', validateToken, (req, res) =>  {
    const output = db.query(getEmployeesQuery,
        (error, result) => {
            if(error) {
                console.log(error)
                res.status(500).json("Error connecting to the database!")
            } else {
                if(result.rowCount != 0) {
                    res.status(200).json(result.rows)
                } else {
                    res.status(404).send("No employee exists!")
                }
            }
        }
    )
})

const getEmployeeQueryByEid = 
" SELECT * \n" +
"FROM Employees emp\n " +
"WHERE empID = $1"

/*
    This /getEmployee/:e_id API also first ensures that there is a valid token before proceeding. Please see validateToken.js for how validateToken works.

    Assuming that there is a valid token, the database is queried to obtain the specified employee's details via the query above. 
    
    Similarly,
    If everything goes okay, all the employees details will be returned in a response with status code 200.
    If there is an error in the connect between the database and backend, the backend will respond with an error code of 500.
    If there is no employee in the database, the backend will respond with an status code of 404.
*/

app.get('/getEmployee/:e_id', validateToken, (req, res) =>  {
    const eid = req.params.e_id;
    const output = db.query(getEmployeeQueryByEid, [eid],
        (error, result) => {
            if(error) {
                console.log(error)
                res.status(500).json("Error connecting to the database!")
            } else {
                if(result.rowCount != 0) {
                    res.status(200).json(result.rows)
                } else {
                    res.status(404).send("Employee #: " + eid + " does not exist!")
                }

            }
        }
    )
})

app.get('/sanityCheck', (req, res) => {
    res.status(200).send("It's working.");
})