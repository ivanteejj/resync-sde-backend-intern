// Pool is the middleware that I'm using to connect the backend and the database
// Something to note is the the host is 'db' instead of 'localhost' since the database is contained
// within its own container with the container name - db.

const Pool = require('pg').Pool
const pool = new Pool({
  user: 'root',
  host: 'db',
  database: 'root',
  password: 'root',
  port: 5432,
})

module.exports = pool;
