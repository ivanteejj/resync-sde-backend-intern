var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var dotenv = require("dotenv")

dotenv.config();

/*
    This function checks whether a user with the specified username exists in the database.
*/
async function doesUserExist(username, db) {
    const query = "select * from users where users.username = $1";
    try {
      const response = await db.query(query, [username]);
      if (response.rowCount === 0) {
        return false;
      } else {
        return true;
      }
    } catch (err) {
        console.log(err)
    }
  }

/*
    This function obtains the details of a specified user in the database.
*/
async function getUserDetails(username, db) {
    const query = "SELECT * FROM Users WHERE Users.username = $1";
    try {
        const response = await db.query(query, [username]);
        if (response.rowCount === 0) {
          return null;
        } else {
            return response.rows[0];
        }
    } catch (err) {
        console.log(err)
    }
}


/*
    This function returns a jwt (token) if the username and password provided are both correct.

    If the user doesn't exist, a response with status code 404 is returned.
    If the the password isn't valid, a response with the status code 401 is returned.
*/
async function login(req, res, db) {
    const username = req.body.username;
    const userExists = await doesUserExist(username, db);

    if(!userExists) {
        return res.status(404).send("User not found.");
    } else {

        // await is used here to ensure that the user details are obtains before proceeding
        const response = await getUserDetails(username, db);
        const hashedPassword = response.userpassword;

        const isPasswordValid = bcrypt.compareSync(req.body.password, hashedPassword);

        if(isPasswordValid) {
            const token = jwt.sign({ id: response.username }, process.env.SECRET_TOKEN, {
                expiresIn: 3600
              });

            res.status(200).send({auth:true, token: token})
        } else {
            res.status(401).send({auth:false, token:null});
        }
    }
}

/*
    This function registers the users into the database.

    If the user already exists, a response with status code 409 is returned.
*/
async function register(req, res, db) {
    const hashedKey = bcrypt.hashSync(req.body.password, 8);

    const userExists = await doesUserExist(req.body.username, db);

    if(userExists) {
        res.status(409).send("Username already exists - please try again");
    } else {
        const updateQuery = "INSERT INTO Users(username, userPassword) VALUES ($1, $2);"

        db.query(updateQuery, [req.body.username, hashedKey],
            (error, result) => {
                if(error) {
                    res.status(500).send("Error connecting to database");
                } else {
                    res.status(200).send("Successfully registered!");
                }
            })
    }
} 

module.exports = {
    register: register,
    login: login
};