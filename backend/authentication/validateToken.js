var jwt = require('jsonwebtoken');
var dotenv = require("dotenv")

dotenv.config();

/*
    A separately defined function since both of the requested APIs will use this function.

    This function will be used to validate the tokens of the request. If the token is verified, the rest of the request may continue running and getting the required information.
    Else, the request will be halted here.

    If there's no token provided, a response with status code 403 is returned.
    If the token's not valid, a response with status code 500 is returned.
*/
function validateToken(req, res, next) {
  var token = req.headers['x-access-token'];

  if (!token) 
    return res.status(403).send({ auth: false, message: 'No token provided.' });

  jwt.verify(token, process.env.SECRET_TOKEN, function(err, decoded) {      
    if (err) 
      return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });    

    next();
  });

}

module.exports = validateToken;
